<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Galleria
 *
 * @author n5pipa00
 */
class Galleria extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        //Haetaan kaikki kansiot
        $data["kansiot"]=$this->kansio_model->hae_kaikki();
        
        //Asetetaan sivupalkki ja sisältönäkymät data-muuttujaan
        $data["sivupalkki"]="kansio/kansiot_view";
        $data["sisalto"]="kuva/kuvat_view";
        //Näytetään template ja viedään tavittavat tiedot näkymiin
        $this->load->view("template.php", $data);
    }
}
